import {
  createStackNavigator,
} from 'react-navigation';
import { OnboardingScreen } from '../containers'
import Dev from './dev'
const App = createStackNavigator(
  {
    Dev: { screen: Dev },
  },
  {
    headerMode: 'none'
  }
);



export default App;