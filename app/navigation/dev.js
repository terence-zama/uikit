import {
    createStackNavigator,
  } from 'react-navigation';
  import { 
    ButtonScreen,
    RootScreen
   } from '../containers/dev'
  
  const Dev = createStackNavigator(
    {
        root: { screen: RootScreen },
        button: { screen: ButtonScreen },
    },
    {
      headerMode: 'none'
    }
  );
  
  
  
  export default Dev;