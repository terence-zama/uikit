import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Button, Container, Text, TextStyle } from '../../component'

export default class Onboarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ScrollView>
          <Button type={'normal'} title={'Normal'} />
          <Button type={'normal'} inverse title={'Normal Inverse'} />
          <Button type={'normal'} secondary title={'Normal Secondary '} />
          <Button type={'normal'} secondary inverse title={'Normal Secondary Inverse '} />
          <Button type={'border'} title={'Border'} />
          <Button type={'border'} title={'Border Secondary'} secondary />
          <Button type={'gradient'} title={'Gradient'} />

          <View style={{ flexDirection: 'row' }}>
            <Button type={'link'} >{'linked'} </Button>
            <Button type={'link'} title={'linked'} />
          </View>
        </ScrollView>
      </Container>
    );
  }
}
