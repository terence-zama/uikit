import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import styles from './styles'
import { Colors } from '../../theme';
import { Text, TextStyle } from '..';
export default class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static Type = Object.freeze({
        Zero: 'zero',
        Normal: 'normal',
        Gradient: 'gradient',
        Border: 'border',
        Link: 'link',
    });

    render() {
        const { title, onPress, highlight, style, type, textStyle, children, inverse, secondary } = this.props
        let ButtonTag = highlight ? TouchableOpacity : TouchableWithoutFeedback;

        switch (type) {
            case Button.Type.Zero: {
                return (
                    <ButtonTag onPress={onPress} style={[
                        style]}>
                        {children}
                    </ButtonTag>
                );
            }
            case Button.Type.Normal: {
                return (
                    <ButtonTag onPress={onPress} style={[
                        styles.buttonContainer,
                        {backgroundColor: inverse? Colors.buttonText : (secondary?Colors.secondary:Colors.primary)},
                        style]}>
                        <Text type={TextStyle.bold} style={[
                            styles.buttonText,
                            {color: inverse?(secondary?Colors.secondary:Colors.primary):Colors.buttonText},
                            textStyle
                            ]}>{title}</Text>
                    </ButtonTag>
                );
            }
            case Button.Type.Border: {
                return (
                    <ButtonTag onPress={onPress} style={[
                        styles.buttonContainer,
                        {
                            backgroundColor: 'transparent',
                            borderWidth:1,
                            borderColor: secondary?Colors.secondary:Colors.primary
                        },
                        style]}>
                        <Text type={TextStyle.regular} style={[
                            styles.buttonText,
                            {color: secondary?Colors.secondary:Colors.primary},
                            textStyle
                            ]}>{title}</Text>
                    </ButtonTag>
                );
            }
            case Button.Type.Gradient: {
                return (
                    <ButtonTag onPress={onPress} style={style}>
                        <View style={styles.gradientButtonContainer}>
                            <LinearGradient
                                style={styles.gradientButton}
                                colors={Colors.gradient}
                                start={{ x: 0.0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
                            >
                                <Text type={TextStyle.bold} style={[styles.buttonText, textStyle]}>{title}</Text>
                            </LinearGradient>
                        </View>
                    </ButtonTag>
                );
            }
            case Button.Type.Link: {
                return (
                    <ButtonTag onPress={onPress} style={[styles.linkContainer, style]}>
                        <Text type={TextStyle.bold} style={[styles.linkText, textStyle]}>{children || title}</Text>
                    </ButtonTag>
                )
            }
        }


    }
}

Button.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    highlight: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    textStyle: PropTypes.object,
    inverse: PropTypes.any,
    secondary: PropTypes.any
}

Button.defaultProps = {
    title: 'Submit',
    highlight: true,
    type: Button.Type.Zero
}
