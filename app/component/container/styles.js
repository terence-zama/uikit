import {StyleSheet,StatusBar, Platform} from 'react-native'
import { Colors } from '../../theme';
const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.background,
        flex: 1,
    },
    body:{
        marginTop: Platform.OS == 'ios'?20:0,
        flex:1
    }
});
export default styles;