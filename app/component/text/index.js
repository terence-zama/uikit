import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types'
const fontFamily = 'Titillium Web'
export const TextStyle = Object.freeze({
    extra_light:'200',
    light:'300',
    regular:'400',
    semi_bold:'600',
    bold:'700',
    black:'900'
})
const ZText = ({
    children,
    type,
    style
}) => {
    
    return (
        <Text style={[{
            fontFamily:fontFamily,
            fontWeight:type,
        },style]}>{children}</Text>
    )
};

ZText.propTypes = {
    type: PropTypes.string.isRequired,

}

ZText.defaultProps = {
    type: TextStyle.regular
}

export default ZText;
