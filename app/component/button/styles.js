import { StyleSheet } from 'react-native'
import { Metrics, Colors } from '../../theme';
const styles = StyleSheet.create({
    buttonContainer:{
        marginRight: Metrics.spacing*2,
        marginLeft: Metrics.spacing*2,
        marginBottom: Metrics.spacing*2,
        height: Metrics.buttonHeight,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Metrics.spacing,
    },
    gradientButtonContainer: {
        marginRight: Metrics.spacing*2,
        marginLeft: Metrics.spacing*2,
        marginBottom: Metrics.spacing*2,
        height: Metrics.buttonHeight + 4,
        backgroundColor: Colors.black + Colors.alpha['20'],
        justifyContent: 'center',
        borderRadius: (Metrics.buttonHeight + 4) / 2,
    },
    gradientButton: {
        marginRight: 2,
        marginLeft: 2,
        height: Metrics.buttonHeight,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Metrics.buttonHeight / 2,
    },
    buttonText: {
        color: Colors.buttonText,
        fontSize: 18,
    },
    linkContainer: {

    },
    linkText: {
        fontSize: 17,
        color: Colors.link,
        alignSelf: 'baseline',
    }
});
export default styles;