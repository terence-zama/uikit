import React from 'react';
import { Text, View, SafeAreaView } from 'react-native';
import styles from './styles'
import { Colors } from '../../theme';
const Container = ({
    children,
}) => (
        <SafeAreaView style={styles.container} backgroundColor={Colors.background}>
            <View style={styles.body}>
                {children}
            </View>
        </SafeAreaView>
    );

export default Container;
